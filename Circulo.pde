class Circulo{
   float h;
   float k;
   float diametro;
   boolean ban;
   
   Circulo(float a, float b, float c){
       h = a; k = b; diametro = c;
       ellipseMode(CENTER);
       ban = false;
   }

   void dibujaCirculo(){
        fill(170);
        if(ban)
           ellipse(h,k,diametro, diametro);
   }
   
      boolean isAdentro(float a, float b){
      return dist(h,k,a,b) <= diametro/2;
   }
}