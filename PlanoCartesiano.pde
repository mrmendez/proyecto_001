class PlanoCartesiano{
  
// *** DECLARACION DE VARIABLES DE INSTANCIA ***
float ancho;
float alto;
float division; // se refiere al espaciado por unidad de los ejes
boolean grid; // true=>se ve el cuadriculado  false=>no se ve el cuadriculado
float lado; // medicion en pixel para el espaciado del cuadriculado
// *** FIN DE DECLARACION DE VARIABLES DE INSTANCIA ***

// El consructor crea y genera el estado del objeto
// es decir a algunas de sus variables de instancia se les asigna los parámetros del constructor
PlanoCartesiano(float a, float b, float c){
    ancho = a;
    alto = b;
    lado = c;  
    grid = false; // grid por default debe ser falso, no visibilidad del cuadriculado.
}

void dibujarPlano(){
     // DIBUJAR EL CUADRICULADO
     if (grid){
         stroke(170);
         strokeWeight(1);
         noFill(); // dibujar solo los bordes del cuadrado
         int cw = (int)(ancho/lado)+2; // numero de cuadros a los ancho
         int ch = (int)(alto/lado)+2; // numero de cuadros a lo alto
         for(int j=0;  j < ch/2  ; j++){
            for(int i=0;  i < cw/2 + 1   ; i++){
               rect(width/2 + i*( lado ), height/2 + j*(lado) , lado, lado);
               rect(width/2 - i*( lado ), height/2 + j*(lado) , lado, lado);
               
               rect(width/2 + i*( lado ), height/2 - (j+1)*(lado) , lado, lado);
               rect(width/2 - i*( lado ), height/2 - (j+1)*(lado) , lado, lado);
               
            } //llave de cierre del for interno
         } // llave de cierre del for anidado
     }

     textSize(16);
     stroke(0); // COLOR NEGRO PARA DIBUJAR LOS EJES
     strokeWeight(2);
     line(0, alto/2, ancho,alto/2);
     pushMatrix();
        translate(ancho, alto/2);
        fill(0); // color negro para las letras y relleno de los triángulos
        triangle(-10,-10, -10,10,0,0);
        triangle(-ancho,0, -ancho+10, 10, -ancho+10, -10);
     popMatrix();
     text("X", ancho-10 , alto/2 +28);
     
     line(ancho/2 , 0, ancho/2, alto);
     pushMatrix();
        translate(ancho/2, 0);
        fill(0); // color negro para las letras y relleno de los triángulos
        triangle(-10, 10, 0, 0, 10, 10);
        triangle(0,alto, -10, alto-10, 10, alto-10);
     popMatrix();
     text("Y", ancho/2-24 ,  14);
     
}
}
