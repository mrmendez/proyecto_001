PlanoCartesiano plano;
ControlTamano ct;
float dx, dy;
boolean ban;

void setup(){
   size(400,400);
   plano = new PlanoCartesiano(width, height, 100);
   ct = new ControlTamano(20,height-30,width-40,20);
   dx = dy = 0;
   ban = false;
}

void draw(){
   background(255);
   plano.dibujarPlano();
   ct.dibujaControl();
}


void mouseClicked(){
    if( !ct.isAdentro(mouseX, mouseY) )
        plano.grid = !plano.grid;
}


void mousePressed(){
     if(ct.circulo.isAdentro(mouseX, mouseY)){
      dx = -(ct.circulo.h - mouseX);
      dy = -(ct.circulo.k - mouseY);
      ban = true;
   }   
}

void mouseDragged(){
       if(ban){
             ct.circulo.h = mouseX - dx;
             if(ct.circulo.h> ( ct.x +ct.circulo.diametro/2)  && ct.circulo.h < ( ct.x+ct.ancho -ct.circulo.diametro/2) ){
               plano.lado=map(ct.circulo.h,(float)(ct.x+ct.circulo.diametro/2), (float)(ct.x+ct.ancho-ct.circulo.diametro/2),10,100);
               //println(""+plano.lado);
             }
    }   
}
void mouseReleased(){
   ban = false;
} 