class ControlTamano{
  float x;
  float y;
  float ancho;
  float alto;
  Circulo circulo;
  int valor=0;
  
  ControlTamano(float a, float b, float c, float d){
      x = a;
      y = b;
      ancho = c;
      alto = d;
      circulo = new Circulo(x+alto/2 , y+alto/2 ,alto);
      circulo.ban = true;
      valor =100;
  }
  
  void dibujaControl(){
    stroke(0);
     if(circulo.h > x+alto/2  &&   circulo.h < ancho+alto/2   )
         circulo.dibujaCirculo();
     else{
        if( circulo.h < (x+alto/2)  ){
          circulo.h = x+alto/2+2; // IZQ
          circulo.dibujaCirculo(); 
        }
        else{
          circulo.h = x+ancho-alto/2; // DER
          circulo.dibujaCirculo();
        }
     } 
         
      noFill();
      stroke(#D31111);
      strokeWeight(2);
      rect(x,y,ancho,alto);
  }
  
  boolean isAdentro(float a, float b){
       return (  a>x && a < (x+ancho)  && b >y && b < (y+alto));
  }  
}